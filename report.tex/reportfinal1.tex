\documentclass[11pt]{article}

% Packages
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{graphicx}
\usepackage[a4paper, margin=2cm]{geometry}
\usepackage[utf8]{inputenc} 
\usepackage{titlesec}
\usepackage{xcolor}

% Referencing
\usepackage[colorlinks=true, linkcolor=blue]{hyperref}

% Mathematics
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{calrsfs}
\usepackage{cleveref}
\usepackage{autonum}
\usepackage{flexisym}
\usepackage{array}
\usepackage{titlesec}
\usepackage{xcolor}
\usepackage{float}
\usepackage{boxedminipage}
\usepackage{textcomp} % certains caracts plus jolies


% Graphics path
\graphicspath{{pdf/}}
\DeclareMathOperator{\R} {\mathbb{R}} 
\DeclareMathOperator{\N} {\mathbb{N}}
\DeclareMathOperator{\C} {\mathbb{C}}

\newtheorem{thm}{Théorème}[section]


\titleclass{\part}{top}
\titleformat{\part}
[display]
{\centering\normalfont\Large}
{\vspace{3pt}\MakeUppercase{\textbf{\partname}}\hfill }
{5pt\titlerule[1.5pt]\vspace{1.0pt}\titlerule[1.0pt]\vspace{1.0pt}\titlerule[0.5pt]\vspace{ 10pt}}
{\Huge}[\vspace{4pt}{\titlerule[1pt]}\vspace{20pt}]

\titlespacing*{\part}{0pt}{0pt}{10pt}

\renewcommand{\vec}{\underline}
\newcommand{\matrixt}[4]{
  \begin{pmatrix}
    #1 & #2\\
    #3 & #4
  \end{pmatrix}
}

\newcommand{\vectort}[4]{
  \begin{pmatrix}
    #1 & #2\\
    #3 & #4
  \end{pmatrix}
}

\newcommand{\constantentry}[4]{
  \\
  \begin{minipage}[t]{.15\textwidth}
    \hfill \emph{$#1$}
  \end{minipage}
  \hfill\vline\hfill
  \begin{minipage}[t]{.80\textwidth}
    \textbf{#2}\quad$\left[#3\right]$
    \vspace{.1cm}
    
    \footnotesize{#4}
  \end{minipage}\\\vspace{.25cm}
}

\newcommand{\lagran}{
  \mathcal{L}
}

\title{Systèmes, Contrôle et Optimisation\\Modélisation physique et analyse}


\author{Mohet Judicaël\\ Roy Nicolas\\ Welcomme Olivier}



\begin{document}

\maketitle

\part{Modélisation}
\section{Description du dispositif}\label{description}
Le modèle simplifié du dispositif \emph{NXTway-GS} avec lequel nous allons travailler par la suite est représenté
à la Figure \ref{firstfig}. Nous allons dans les sections suivantes décrire les différentes
grandeurs physique
définissant le modèle. 
\begin{figure}[h]
  \centering
  \input{pdf/nxtgs.pdf_tex}
  \caption{\textbf{Schéma de principe du dispositif NXTway-GS.} 
  Différentes caractéristiques géométriques sont représentées.}
  \label{firstfig}
\end{figure}



\begin{figure}
  \centering
  \def\svgwidth{0.90\linewidth}
  \input{pdf/dessin.pdf_tex}
  \caption{\textbf{Projection de coupe et horizontale du robot.} 
  Les positions des centres de masse du robot sont représentées en pointillés. Le centre de masse du
  corps est indiqué par la cible.}
  \label{secondfig}
\end{figure}




\newpage
\subsection{Roues}
Le robot possède deux roues identiques décrites par un disque d'une masse et d'un rayon donnés\\
\constantentry{m_w}{0.03}{kg}{Mass of one wheel,}\\
\constantentry{r_w}{0.04}{m}{Wheel radius.}\\
Sur base de ces constantes, on peut déterminer le moment d'inertie de la roue
\begin{equation}
  j_w=\frac{m_rr_w^2}{2}.
\end{equation}
\subsection{Corps du robot}
Les définitions suivantes décrivent la géométrie du corps du robot.\\
\constantentry{m_b}{0.6}{kg}{Body mass}\\
\constantentry{w_b}{0.14}{m}{Body width}\\
\constantentry{d_b}{0.04}{m}{Body depth}\\
\constantentry{h_b}{0.144}{m}{Body height}\\
Cette fois, le corps est une pièce soumise à la rotation de $\psi$ et de $\phi$ son moment d'inertie s'écrit
\begin{equation}
    j_\psi=\frac{m_bh_b^2}{3}
\end{equation}
\begin{equation}
    j_\phi=\frac{m_b\left(w_b^2+d_b^2\right)}{12}
\end{equation}
sur ces axes de rotation.


\subsection{Moteur DC}
Les roues du robot sont placées sur le moyeu d'un moteur alimenté par une tension DC.\\
\constantentry{j_m}{0.00001}{kg m^2}{Moment d'inertie du moyeu du moteur}\\
La force produite par le moteur sera par la suite exprimée en fonction de la tension appliquée.
Pour ce faire, nous avons besoin des constantes suivantes:
\constantentry{r_m}{6.69}{\Omega}{Résistance Ohmique effective du moteur}\\
\constantentry{k_b}{0.468}{\frac{Vs}{rad}}{Force contre-électromotrice}\\
\constantentry{k_c}{0.317}{\frac{Nm}{A}}{Couple du moteur}\\
Les roues sont couplées aux moteurs avec un ratio unitaire.\\
\constantentry{n}{1.0}{1}{Ratio entre le nombre de dents de l'engrenage roue sur celui de
l'engrenage moteur}\\
Il est nécessaire de tenir compte des frictions à l'intérieur du moteur, cette force vient affecter
le couplage entre le tangage du corps du robot et la rotation de ses roues.\\
\constantentry{f_m}{0.0022}{\frac{Ns}{rad}}{Coefficient de friction angulaire entre le corps et le moteur}\\
Nous considérons que dans la gamme de vitesse angulaire appliquée aux roues du robot et l'adhérence
des roues, les forces de
friction n'interviennent pas entre les roues et le sol (couplage entre roues et sol idéal).\\
\constantentry{f_w}{0.0}{\frac{Ns}{rad}}{Coefficient de friction angulaire avec le sol}\\
\subsection{Autres constantes}
\quad\\
\constantentry{g}{9.81}{m/sec^2}{Accélération gravitationnelle sur terre}

\section{Équations du mouvement}
Considérons le système présenté à la Figure \ref{secondfig}, trois centres de masse $\vec{x_l}$, $\vec{x_r}$ et
$\vec{x_b}$ sont présents pour le corps du robot et les deux roues.

La première étape consiste à réduire le nombre de variable afin d'obtenir les coordonnées
généralisées.
Une représentation efficace du système peut être
formulée sur base des angles
\begin{equation}
  \begin{split}
    \theta & \quad \text{:\quad Angle moyen des deux roues} \\
    \psi   & \quad \text{:\quad Angle de tangage} \\
    \phi   & \quad \text{:\quad Angle de pivot.} 
  \end{split}
\end{equation}
Pour parvenir à cette représentation du système, les centres de masses seront localisés par rapport
au point $\vec{x}_m$, fixé entre les roues du robot, centre de tangage et pivot.

\begin{equation}
  \begin{split}
    \vec{\dot{x}_m} &= \left(r_w\dot{\theta}\cos{\phi}, r_w\dot{\theta}\sin{\phi}\right) \\
    \vec{x_m} &= \left(\int \dot{x}_m dt, \int \dot{x}_m dt, r_w\right) \\
    \vec{x_l} &= \left(x_m-\frac{w_b}{2}\sin{\phi}, y_m+\frac{w_b}{2}\cos{\phi}, z_m\right)\\
    \vec{x_r} &= \left(x_m+\frac{w_b}{2}\sin{\phi}, y_m-\frac{w_b}{2}\cos{\phi}, z_m\right)\\
    \vec{x_b} &= \left(x_m+\frac{h_b}{2}\sin{\psi}\cos{\phi}, y_m+\frac{h_b}{2}\sin{\phi}\sin{\psi},
    z_m+\frac{h_b}{2}\cos{\psi}\right)\\
  \end{split}
\end{equation}
\subsection{Équations de Lagrange}
Nous allons employer le formalisme Lagrangien afin de dériver les équations dynamiques du système.
Pour cela, nous devons identifier les contributions à l'énergie du système et écrire la fonction de
Lagrange
\begin{equation}
  \lagran = T-V
\end{equation}
avec T représentant l'énergie cinétique du système et V représentant son énergie potentielle.
L'énergie cinétique se compose de deux contributions, d'une part la contribution des centres de
masse
\newcommand{\udemi}{\frac{1}{2}}
\begin{equation}
  T_1=\udemi m_w\left(\dot{x}_l^2 + \dot{y}_l^2 + \dot{z}_l^2\right) + \udemi m_w\left(\dot{x}_r^2
  + \dot{y}_r^2 + \dot{z}_r^2\right) + \udemi m_b\left(\dot{x}_b^2 + \dot{y}_b^2 + \dot{z}_b^2\right)
\end{equation}
\begin{equation}
  T_2=\udemi j_w \dot{\theta}_r^2+ \udemi j_w \dot{\theta}_l^2+\udemi j_\psi\dot{\psi}^2+\udemi
  j_\phi\dot{\phi}^2+\udemi n^2j_m\left(\dot{\theta}_l-\dot{\psi}\right)^2+\udemi
  n^2j_m\left(\dot{\theta}_r-\dot{\psi}\right)^2
\end{equation}
\begin{equation}
  V = m_wgz_l+m_wgz_r+m_bgz_b
\end{equation}
Nous pouvons dés lors exprimer la dynamique via le système d'équations de Lagrange
associé aux variables généralisées
\begin{equation}
  \frac{d}{dt}\left(\frac{\partial \lagran}{\partial  \dot{q}_j}\right)-\frac{\partial \lagran}{\partial q_j}
  = Q_j.
\end{equation}
Nous obtenons les expressions suivantes:
\begin{equation}
  \begin{split}
\frac{d}{dt}\left(\frac{\partial \lagran}{\partial  \dot{\theta}}\right)-\frac{\partial
  \lagran}{\partial \theta}
    &= \left[\left(2m_w+m_b\right)r_w^2+2j_w+2n^2j_m\right]\ddot{\theta} \\ % ok
    &+ \left[m_b\frac{h_b}{2}r_w\cos{\psi}-2n^2j_m\right]\ddot{\psi} \\ % ok
    &- \left[m_b\frac{h_b}{2}r_w\sin{\psi}\right]\dot{\psi}^2 
  \end{split}
\end{equation}

\begin{equation}
  \begin{split}
    \frac{d}{dt}\left(\frac{\partial \lagran}{\partial  \dot{\psi}}\right)-\frac{\partial
    \lagran}{\partial \psi} 
    &=\left[m_b\frac{h_b}{2}r_w\cos{\psi}-2n^2j_m\right]\ddot{\theta} \\
    &+\left[m_b\frac{h_b^2}{4}+j_\psi+2n^2j_m\right]\ddot{\psi} \\
    &-m_b\frac{h_b^2}{4}\sin{\psi}\cos{\psi}\dot{\phi}^2\\
    &-m_bg\frac{h_b}{2}\sin{\psi}
  \end{split}
\end{equation}

\begin{equation}
  \begin{split}
    \frac{d}{dt}\left(\frac{\partial \lagran}{\partial  \dot{\phi}}\right)-\frac{\partial
    \lagran}{\partial \phi} 
    &= \left[\frac{m_ww_b^2}{2}+j_\phi+\frac{w_b^2}{2r_w^2}\left(j_w+n^2j_m\right)
    +m_b\frac{h_b^2}{4}\sin^2{\psi}\right]\ddot{\phi}\\
    &+2m_b\frac{h_b^2}{4}\dot{\psi}\dot{\phi}\sin{\psi}\cos{\psi}
  \end{split}
\end{equation}
\subsection{Forces appliquées sur le système}
Nous devons à présent évaluer le membre de droite du système d'équations de Lagrange $G_j$
représentant la résultante de forces projetée sur la coordonnée généralisée $q_j$.

Nous commençons par énumérer les différentes forces en présence.
La première force à laquelle nous pensons est la force gravitationnelle, cependant cette force
conservative est déjà prise en compte dans le formalisme Lagrangien employé.
En effet, le poids est le gradient d'un potentiel gravitationnel inclus dans $V$.

La seconde force que nous relevons est le moment appliqué aux niveau des roues par les deux moteurs
DC. Cette force s'exprime
\begin{equation}
  F_{em}=nk_ci
\end{equation}
Enfin, le troisième groupe de forces à prendre en compte est la friction. Ces forces non
conservatives interviennent comme multiple d'une vitesse et ne participent pas au transfert
d'énergie potentielle en énergie cinétique et inversement
\begin{equation}
  F_{f}=f\dot{\omega}, \omega \in \mathbb{R}.
\end{equation}

En tenant compte des frictions entre le moteur et la roue, la roue et le sol, nous parvenons
à l'expression suivante pour le couple résultant appliqué aux roues.
\begin{equation}
  \begin{split}
  F_l=nk_ci_l+f_m\left(\dot{\psi}-\dot{\theta}_l\right)-f_w\dot{\theta}_l \quad\text{à gauche}, \\
  F_r=nk_ci_r+f_m\left(\dot{\psi}-\dot{\theta}_r\right)-f_w\dot{\theta}_r \quad\text{à droite}.
  \end{split}
\end{equation}
En réaction, une force équivalente est appliquée au corps du robot:

\begin{equation}
  F_\psi = -nk_c \left(i_l+i_r\right) -f_m\left(2\dot{\psi}-\dot{\theta}_l-\dot{\theta}_r\right)
  + f_w\left(\dot{\theta}_l+\dot{\theta}_r\right).
\end{equation}

Finalement, le long des coordonnées généralisées, nous avons

\begin{equation}
  \begin{split}
    G_\theta &= F_l+F_r, \\
    G_\psi &= F_\psi, \\
    G_\phi &=\frac{w_b}{2r_w}\left(F_r-F_l\right).
  \end{split}
\end{equation}
\begin{equation}
\end{equation}
\subsubsection{Force du moteur DC}
Le moteur employé est basé sur un contrôle PWM, c'est à dire que le contrôle s'effectue via une
tension pulsée dont la largeur d'impulsion est liée au voltage effectivement appliqué.
Or nous avons jusqu'à présent exprimé le couple du moteur via le courant $i$.
Nous employons donc l'équation des moteurs à courant continu 
\begin{equation}
  L_m\dot{i}_{l,r} = v_{l,r} + k_b\left(\dot{\psi}-\dot{\theta}_{l,r}\right) -r_mi_{l,r}
\end{equation}
afin d'exprimer le courant en fonction de la tension de contrôle $v$. Nous obtenons pour $i$,
\begin{equation}
  i_{l,r} = \frac{v_{l,r} + k_b\left(\dot{\psi}-\dot{\theta}_{l,r}\right)}{r_m}.
\end{equation}
\subsection{Points fixes du système}
Le système d'équations admet deux points fixes: lorsque le pendule inversé dans un
équilibre stable pour $\psi=\pi$ et lorsqu'il est dans un équilibre instable en $\psi=0$.
Le premier point fixe n'a pas d'intérêt technologique, notre objectif étant de maintenir le robot en
équilibre sur ses roues. En effet, ce point fixe n'est pas disponible en pratique car la collision
avec le sol lors de la chute du robot sur un sol plat
ne permet pas des angles $\psi$ tels que $|\psi|>\pi-\cos^{-1}{\frac{r_w}{h_b}}$.
\subsection{Linéarisation}
Les équations sont linéarisées autour du point fixe instable. Ce qui signifie que nous prendrons l'hypothèse 
suivante à proximité du point fixe: $\psi\rightarrow 0$, ce qui implique que $\psi^2 \rightarrow 0$,
$\cos{\psi}\rightarrow 1$ et $\sin{\psi}\rightarrow 0$. Cette hypothèse de linéarisation nous
débarrasse des fonctions non linéaires des variables généralisées. Nous
pouvons désormais écrire la dynamique à proximité du point fixe instable sous forme d'un système linéaire
d'équations différentielles:

\begin{equation}
  E
  \begin{pmatrix}
  \ddot{\theta}\\ \ddot{\psi}
  \end{pmatrix}
  +F
  \begin{pmatrix}
  \dot{\theta}\\ \dot{\psi}
  \end{pmatrix}
  +G
  \begin{pmatrix}
  {\theta}\\ {\psi}
  \end{pmatrix}
  = H
  \begin{pmatrix}
  {v_l}\\ {v_r}
  \end{pmatrix},
  \label{eqtang}
\end{equation}
pour $\psi$ et $\theta$ et
\begin{equation}
  I\ddot{\phi}+J\dot{\phi}=K\left(v_r-v_l\right)
  \label{eqyaw}
\end{equation}
pour $\phi$ qui se retrouve découplé de la dynamique de $\theta$ et $\psi$ mais toujours soumis aux
mêmes forces extérieures. En effet, la linéarisation indique que près du point fixe, le mouvement de
pivot est directement relié aux forces appliquées aux roues sans intervention de l'inclinaison
considérée faible. De son côté, l'équilibre du robot est contenu dans le système \ref{eqtang} uniquement.

\section{Définition du système $R$}
La section précédente nous a laissé avec un système physique dont la dynamique de
tangage (ou d'inclinaison) est décrite par le système d'équations différentielles linéaires d'ordre deux à deux dimensions
\begin{equation}
  E
  \begin{pmatrix}
  \ddot{\theta}\\ \ddot{\psi}
  \end{pmatrix}
  +F
  \begin{pmatrix}
  \dot{\theta}\\ \dot{\psi}
  \end{pmatrix}
  +G
  \begin{pmatrix}
  {\theta}\\ {\psi}
  \end{pmatrix}
  = H
  \begin{pmatrix}
  {v_l}\\ {v_r}
  \end{pmatrix},
  \label{eqtang}
\end{equation}
sa dynamique de lacet ou son orientation est quant à elle exprimée par un système de même ordre
à une dimension, indépendant de $\theta$ et $\psi$
\begin{equation}
  I\ddot{\phi}+J\dot{\phi}=K\left(v_r-v_l\right).
  \label{eqyaw}
\end{equation}
L'indépendance de \ref{eqyaw} nous permet de facilement combiner les deux systèmes en un
seul, d'ordre 2 à trois dimensions. Ce système global s'obtient en greffant un bloc relatif
à \ref{eqyaw} dans les matrices E, F et G dans \ref{eqtang} comme suit. La matrice H quant à elle
verra uniquement son rang augmenter alors que nous ajoutons le terme indépendant de \ref{eqyaw}.

\begin{equation}
  \begin{pmatrix}
    E_{11}&E_{12}&0\\
    E_{21}&E_{22}&0\\
    0&0&I
  \end{pmatrix}
  \begin{pmatrix}
    \ddot{\theta}\\ \ddot{\psi} \\ \ddot{\phi}
  \end{pmatrix}
   \begin{pmatrix}
    F_{11}&F_{12}&0\\
    F_{21}&F_{22}&0\\
    0&0&J
  \end{pmatrix}
  \begin{pmatrix}
    \dot{\theta}\\ \dot{\psi} \\ \dot{\phi}
  \end{pmatrix}
  \begin{pmatrix}
    G_{11}&G_{12}&0\\
    G_{21}&G_{22}&0\\
    0&0&0
  \end{pmatrix}
  \begin{pmatrix}
    {\theta}\\ {\psi}\\ {\phi}
  \end{pmatrix}
  =
  \begin{pmatrix}
    H_{11}&H_{12}\\
    H_{21}&H_{22}\\
    -K&K
  \end{pmatrix}
  \begin{pmatrix}
  {v_l}\\ {v_r}
  \end{pmatrix}.
  \label{eqsystem3}
\end{equation}

Maintenant que nous avons exprimé la dynamique en un seul système linéaire représenté par les
matrices $\hat{E}$, $\hat{F}$, $\hat{G}$ et $\hat{H}$, nous souhaitons l'amener
à une représentation du premier ordre correspondante à la description d'un système de contrôle
\begin{equation}
  \dot{\vec{x}} = A\vec{x} + B \vec{u}.
\end{equation}
Pour parvenir à cette représentation, nous allons faire passer le système d'EDO(2)\footnote{Cette
notation EDO(N) signifie Équation Différentielle d'ordre N.} à 3 dimensions
à un système d'EDO(1) à 6 dimensions.

\subsection{Réduction de l'ordre du système linéaire d'équations différentielles}

Afin de garder des notations claires, considérons un système d'EDO(2) quelconque, nous définirons le
contenu de ces matrices pour notre système par la suite.
\begin{equation}
  \ddot{\vec{x}} = M\dot{\vec{x}} + N\vec{x} + \vec{x_c},
\end{equation}
tel que $M, N \in \mathbb{R}^{nxn}$, $P\in\mathbb{R}^{nxp}$ et $\vec{x_c}=P\vec{u}\in\mathbb{R}^n$.\\
Posons une nouvelle variable $y = \dot{\vec{x}}$, nous avons que
\begin{gather}
  \dot{\vec{x}} = \vec{y} \\ 
  \dot{\vec{y}} = N\vec{x} + M\vec{y}+ \vec{x_c}.
\end{gather}
Ce système à 2n dimensions peut se représenter sous forme de matrices
\begin{equation}
  \begin{pmatrix}
    \dot{\vec{x}} \\ \dot{\vec{y}}
  \end{pmatrix}
  =
  \begin{pmatrix}
    \mathbb{O}_{nxn}&\mathbb{I}_{nxn}\\
    N&M
  \end{pmatrix}
  \begin{pmatrix}
    \vec{x} \\ \vec{y}
  \end{pmatrix}
  +
  \begin{pmatrix}
    \vec{0} \\ \vec{x_c}
  \end{pmatrix}
\end{equation}

Dans le cas du système \ref{eqsystem3}, en considérant que
\begin{equation}
  \begin{vmatrix}
    E_{11}&E_{12}&0\\
    E_{21}&E_{22}&0\\
    0&0&I
  \end{vmatrix}
  \neq0,
\end{equation}
et
\begin{equation}
  \vec{x}
  =
  \begin{pmatrix}
    \theta \\ \psi\\ \phi
  \end{pmatrix}
\end{equation}
nous pouvons écrire:
\begin{equation}
  \begin{pmatrix}
    \dot{\vec{x}}\\
    \ddot{\vec{x}}   
  \end{pmatrix}
  =
  \matrixt
  {\mathbb{O}_{3x3}}      {\mathbb{I}_{3x3}}
  {- \hat{E}^{-1}\hat{G}} {-\hat{E}^{-1} \hat{F}}
  \begin{pmatrix}
    \vec{x} \\ \dot{\vec{x}}
  \end{pmatrix}
  +
  \begin{pmatrix}
    \mathbb{O}_{3x2}\\
    \hat{E}^{-1}\hat{H}
  \end{pmatrix}
  \vec{u}
\end{equation}
étant donné que
\begin{equation}
    \vec{x_c} = P\vec{u} =   \hat{E}^{-1} \hat{H}\vec{u}.
\end{equation}
Cette formulation nous permet bien de représenter le système sous forme du vecteur d'état
\begin{equation}
   \begin{pmatrix}
    \vec{x}\\
    \dot{\vec{x}}   
  \end{pmatrix} 
  =
  \begin{pmatrix}
    \theta\\
    \psi\\
    \phi\\
    \dot{\theta}\\
    \dot{\psi}\\
    \dot{\phi}
  \end{pmatrix}.
\end{equation}
\subsection{Choix de la matrice C}
Plutôt que de prendre une matrice $C$ identité, nous avons choisi de restreindre nos capteurs à une
mesure de de distance angulaire parcourue pour nos trois coordonnées généralisées, ce qui se traduit
par la matrice suivante:
\begin{equation}
  C = 
  \begin{pmatrix}
    1& 0& 0& 0& 0& 0\\
    0& 1& 0& 0& 0& 0\\
    0& 0& 1& 0& 0& 0
  \end{pmatrix}.
\end{equation}

L'impact du choix de la matrice $C$ sera présenté dans la section concernant
l'observabilité du système.

\subsection{Deuxième méthode de calcul pour A et B}
Il peut être intéressant d'obtenir et d'analyser d'autres alternatives dans le cadre du calcul des deux matrices $A$ et $B$ sur base des matrices $A_1$, $A_2$, $B_1$ et $B_2$.\\

Pour se faire, nous nous baserons tout d'abord sur l'expression suivante :

\begin{equation}
	\dot{x}=Ax
	\label{EqEntreeNulle}
\end{equation}\\

Celle-ci nous permet donc d'évaluer $\dot{x}$ en considérant une entrée nulle. Nous pourrons étendre cette expression sur $A_1$ et $A_2$ afin de pouvoir évaluer à la fois $x_1$ et $x_2$. Les expressions alors obtenues seront semblables à \ref{EqEntreeNulle}, nous donnant ainsi :

\begin{equation}
\dot{x_1}=A_1x_1
\label{Eq1EntreeNulle}
\end{equation}
\begin{equation}
\dot{x_2}=A_2x_2
\label{Eq2EntreeNulle}
\end{equation}\\

Dès lors que nous connaissons les valeurs de $x_1$ et $x_2$, ainsi que celles des deux matrices $A_1$ et $A_2$, il nous est dès lors possible de calculer l'expressions des deux vecteurs $\dot{x_1}$ et $\dot{x_2}$ à partir des formules \ref{Eq1EntreeNulle} et \ref{Eq2EntreeNulle}.

Les vecteurs $x_1$ et $x_2$ ayant à la base l'expression suivante :\\

\begin{equation}
x_1 = 
\begin{bmatrix}
\theta,  &\psi , &\dot{\theta} , &\dot{\psi}
\end{bmatrix}^T
\end{equation}
\begin{equation}
x_2 = 
\begin{bmatrix}
\phi,   &\dot{\phi} 
\end{bmatrix}^T
\end{equation}
\\[0.3cm]

nous sommes en mesure de créer un nouveau vecteur $x$ dont le but sera de combiner les deux vecteurs $x_1$ et $x_2$, afin de pouvoir obtenir l'expression $\dot{x}=Ax$. Ce nouveau vecteur $x$ sera semblable au suivant :\\
\begin{equation}
x = 
\begin{bmatrix}
x_1\\x_2 
\end{bmatrix}
=
\begin{bmatrix}
\theta,  &\psi , &\dot{\theta} , &\dot{\psi}, &\phi,   &\dot{\phi} 
\end{bmatrix}^T
\end{equation}
\\[0.3cm]
 
 Nous établirons donc l'expression de $\dot{x}$ de la façon suivante :\\
 
 
\begin{equation}
x = 
 \begin{bmatrix}
 \dot{\theta},  &\dot{\psi} , &\ddot{\theta} , &\ddot{\psi}, &\dot{\phi},   &\ddot{\phi}
 \end{bmatrix}^T
 \end{equation}
 \\[0.3cm]
 
 Or, nous avons calculé précédemment l'expression des composants de $\dot{x}$ à l'aide des formules \ref{Eq1EntreeNulle} et \ref{Eq2EntreeNulle}.\\
 
 Il ne nous reste donc plus qu'à trouver la matrice $A$ de telle manière que l'expression $\dot{x}=Ax$ soit vérifiée, connaissant à présent les valeurs de $x$ et $\dot{x}$.\\
 
 Cela effectué, nous avons désormais trouvé la nouvelle matrice $A$ correspondant au vecteur $x$. La matrice $B$ pourra être obtenue suivant le même raisonnement, nous permettant ainsi d'avoir les matrices $A$ et $B$ du système $R$.



\section{Description externe}

La déscription externe d'un système $R=[A, B, C, D]$ consiste à décrire le système sans s'intéresser aux trajectoires. D'un point de vue temporel, on peut le décrire via sa matrice impulsionnelle:
\[
G = C e^{At} B
\]
pour autant que la matrice $D$ soit nulle. On a alors, pour $t\in \R$:
\[
y(t) = (G \ast u)(t)
\]
D'autre part, d'un point de vue fréquentielle, on détermine la fonction de transfert qui est définie comme étant la transformée de Laplace de la matrice impulsionnelle
\[
\hat{G}(s) = C (sI - A)^{-1} B,~~~~~s \in \C
\]
Pour le système du robot NXTway-GS, la fonction de transfert est donnée par l'expression suivante:

\[
s \mapsto
\begin{pmatrix}
\dfrac{157.8 s^2 - 1.138 * 10^4}{s^4 + 240.6 s^3 - 269.6 s^2 - 1.171 * 10^4 s}  & \dfrac{-76.07 s}{s^3 + 240.6 s^2 - 269.6 s -1.171 * 10^4} & \dfrac{-53.16}{s^2 + 95.7 s}\\
\dfrac{157.8 s^2 - 1.138 * 10^4}{s^4 + 240.6 s^3 - 269.6 s^2 - 1.171 * 10^4 s} & \dfrac{-76.07 s}{s^3 + 240.6 s^2 - 269.6 s -1.171 * 10^4}  & \dfrac{53.16}{s^2 + 95.7 s}
\end{pmatrix}
\]



\part{Analyse}
Dans cette partie, une analyse de la stabitlité, contrôlabilité, observabilité, détéctabilité et stabilisabilité sera faite sur base de la théorie pour le système du robot NXTway-GS présenté dans la section \ref{description}.  
\section{Analyse dynamique du modèle}
\subsection{Stabilité interne et externe}
Pour vérifer la stabilité interne d'un système dynamique $ R =  [A, B, C, D] $ donné, il suffit de verifier que le spectre de $A$ est inclus dans le demi-plan complexe $\C_{-}$ ouvert, c'est à dire $\{ z \in \C | \Re(z) < 0 \}$. 
Dans le cas du sytème du robot, on vérifie facilement que le système n'est pas internement stable. On peut dés lors construire les sous-espaces anti-stable, stable, et instable en fonction des signes des parties réelles des  valeurs propres de la matrice $A$: 


\begin{align}
\mathcal{L}^{+}(A)&=span\left( \left(
\begin{array}{c}
-0.0712\\
-0.1127\\
0\\
-0.5293\\
-0.8379\\
0
\end{array}
\right) \right)\\
\mathcal{L}^{-}(A)&= span\left(\left(
\begin{array}{c}
-0.0037\\
0.0018\\
0\\
0.9006\\
-0.4346\\
0
\end{array}
\right) ,~~
\left(
\begin{array}{c}
-0.01247\\
-0.0863\\
0\\
0.8128\\
0.5625\\
0
\end{array}
\right), ~~
\left(
\begin{array}{c}
0\\
0\\
-0.0104\\
0\\
0\\
0.9999
\end{array}
\right)
\right)\\
\mathcal{L}^{0+}(A)&= span \left( \left(
\begin{array}{c}
-0.0712\\
-0.1127\\
0\\
-0.5293\\
-0.8379\\
0
\end{array}
\right),~~
e_1,~~ e_3
\right)
\end{align}


On peut remarquer que la dimension de $\mathcal{L}^{-}(A)$ vaut $ 3 < 6$ se qui corrobore bien nos conclusions sur la stabilité interne. 

Concernant la stabilité externe, il faut vérifier que la fonction de transfert du système $R$ est holomorphe sur le demi-plan complexe $\C^+$, i.e sur $\{ z \in \mathcal{P}(\hat{G}) | \Re(z) < 0\}$, où $\mathcal{P}(\hat{G})$ est l'ensemble des pôles de la fonction de transfert. Cela revient en faite à vérifier que les parties réelles de tous les pôles de la fonction de transfert sont strictement négatives. Dans notre cas, on se rend compte que la fonction de tranfert a 4 pôles qui posent problème et donc le système étudié n'est pas externement stable.
\subsection{Contrôlabilité}
Concernant la contrôlabilité d'un système $R$, plusieurs tests sont possibles. Le premier consiste à vérifier que la matrice de contrôlabilité est de rang plein. Si cela n'est pas le cas, on peut effectuer les tests modeaux pour déterminer les modes qui ne sont pas contrôlable. 
Dans le cas du segway, la première méthode nous confirme que le système est complétement contrôlable. On a donc que le sous-espace contrôlable est ${\R}^{6}$.
\subsection{Observabilité}
Pour déterminer si un système $R$ est observable, il y a là aussi plusieurs possibilités. De manière analogue au cas précédent, on peut vérifier que la matrice d'observabilité est de rang plein. Ce qui est le cas pour notre système. 

Il faut tout de même se rappeler que la propriété d'observabilité depend du choix de la matrice $C$ de départ. Dans notre cas, si $C$ est pris comme l'idendité , alors le système est observable. Cependant, si on prend $C$ pour n'avoir que les positions en sortie, i.e
\begin{equation}
  C = 
  \begin{pmatrix}
    1& 0& 0& 0& 0& 0\\
    0& 1& 0& 0& 0& 0\\
    0& 0& 1& 0& 0& 0
  \end{pmatrix}
\end{equation}

alors le système reste complétement observable.

En ce qui concerne le sous-espace inobservable, il est simplement réduit à $0$. Ce qui signifique que le seul état inobservable est l'état nul $x_0 =0$. 
\subsection{Stabilisabilité}
En guise de rappels, nous savons que notre système sera stable si tout espace instable est contrôlable. Autrement dit, nous désirons savoir si tous les vecteurs propres associés à des valeurs propres positives feront bel et bien partie du sous-espace contrôlable.\\

\noindent \begin{boxedminipage}[pos]{\textwidth}
	\begin{thm}	 \textbf{\emph{[Stabilisabilité]}}
		\begin{center}
		Le système est stabilisable \\
		$\Leftrightarrow$ \\
		le sous-espace instable  $ L^{o+}$ $\subset$ C(A,B) \\
		$\Leftrightarrow$\\
		 S(A,B) = $ \Re^{n}$.
	 \end{center}
	\end{thm}
\end{boxedminipage}
\medskip\\

Dès lors, notre système étant complètement contrôlable, il est aisé de savoir qu'il sera également entièrement stabilisable.


\subsection{Détectabilité}
A nouveau, nous rappellerons que notre système sera stable si tout espace instable est observable. Autrement dit, nous désirons savoir si tous les vecteurs propres associés à des valeurs propres positives feront bel et bien partie du sous-espace observable.\\

\noindent \begin{boxedminipage}[pos]{\textwidth}
	\begin{thm}	\textbf{\emph{[Détectabilité]}}
		\begin{center}
			Le système est (exponentiellement) détectable \\
			$\Leftrightarrow$ \\
			le système est (exponentiellement) stable, ou si la partie instable est complètement observable. \\
			$\Leftrightarrow$ \\
			Le système est complètement observable.
		\end{center}
		
	\end{thm}
\end{boxedminipage}
\medskip\\

Ainsi, notre système étant complètement observable, nous en conclurons qu'il est également détectable.


\section{Tests dynamiques en boucle ouverte}

\subsection{Trajectoires d'état libres}


Dans cette section, nous allons présenter différentes trajectoires, dites d'état libres, ce qui signifie que l'entrée $u$ est identiquement nulle. Pour ne pas encombrer ce texte, nous ne les illustrerons que pour trois modes, un stable, un instable et un "antistable", i.e de valeure propre associée nulle.

 \subsubsection{Trajectoires liées au mode stable}
 
En premier lieu, la représentation de la trajetoire du mode associé au vecteur propre $v_1$, où
\[
v_1 = \left( \begin{array}{c}
-0.0037\\
0.0018\\
0\\
0.9006\\
-0.4346\\
0
\end{array}\right)
\]
est donnée par la figure $\ref{stable}$:
\begin{figure}[H]
	\centering
	\def\svgwidth{0.75\linewidth}
	\input{pdf/stable.pdf_tex}
	\caption{\textbf{Trajectoire d'état libre correspondant à un mode stable}}
	\label{stable}
\end{figure}
C'est donc un mode stable, ce qui est logique étant donné que $v_1\in \mathcal{L}^{-}(A)$. De plus, on a 
\[
x(t) = e^{\lambda_1 t} v_1 = e^{-241.5197 t}v_1
\]
On remarque bien que $x(t)$ tend vers $0$ si $t$ tend vers $+ \infty$. On peut aussi remarquer que la vitesse de convergence est assez grande étant donné la valeur élevée du module de $\lambda_1$. Qui plus est, le vecteur d'état s'écrit:
\[
x(t) = \left( \begin{array}{c}
\theta \\
\psi \\
\phi \\
\dot \theta \\
\dot \psi \\
\dot \phi 
\end{array}
\right)
\]

Dans le cas de $v_1$, on a $ \phi $ et $\dot \phi $ nuls, ce qui signifie qu'il n'y a pas de mouvement de précession (rotation autour de l'axe vertical). Par contre, $\theta $ croît vers $0$ tandis que $ \psi $ décroît vers la même valeur. Physiquement, les roues sont décalées par rapport à l'origine et sont ramenées rapidement ($\approx 0.015 s$) vers l'origine. Ce mouvement compense alors l'inclinaison du corps qui est ramené en position d'équilibre à l'origine.

 \subsubsection{Trajectoires liées au mode instable}
 
 En second lieu vient la trajectoire générée par un mode instable. Le vecteur propre $v_2$ est quant à lui donné par:
 \[
 v_2 = \left( \begin{array}{c}
 -0.0712\\
 -0.1127\\
 0\\
 -0.5293\\
 -0.8379\\
 0
 \end{array}\right)
 \]
 On obtient alors les trajectoires suivantes \ref{instable} :
 \begin{figure}[H]
 	\centering
 	\def\svgwidth{0.75\linewidth}
 	\input{pdf/instable.pdf_tex}
 	\caption{\textbf{Trajectoire d'état libre correspondant à un mode instable}}
 	\label{instable}
 \end{figure}
 
 Danc ce cas, on obtient:
 \[
 x(t) = e^{7.4361 t}v_2
 \]
 Se qui tend vers l'infini pour $t$ assez grand. Ces conclusions sont logiques étant donné que les vitesses angulaires $\dot \theta $ et $\dot \psi $ ainsi que les angles associés sont negatifs. Le robot est donc incliné et le mouvement accentue cet effet jusqu'à la chute du système.
 Encore une fois, il n'y a pas de mouvement de précession.
 
  \subsubsection{Trajectoires liées au mode antistable}
 
 Et enfin, le mode constant qui correspond à la valeur propre nulle et au vecteur propre $v_3 = e_1$ donne \ref{constant}
 
 \begin{figure}[H]
 	\centering
 	\def\svgwidth{0.75\linewidth}
 	\input{pdf/constant.pdf_tex}
 	\caption{\textbf{Trajectoire d'état libre correspondant à un mode constant}}
 	\label{constant}
 \end{figure}
 
 Danc cette configuration, la trajectoire est simplement donnée par $x(t) = e_1$. En faite, l'angle $\theta$ est décalé par rapport à l'origine et reste constant tandis que tous les autres dimensions restent constamment nulles. Le robot se repose donc sur ses lauriers.
 
 \section{Réponse impulsionnelle}
 
La réponse impusionnelle consiste à choisir l'entrée $u$ comme étant la fonction delta de dirac $ t \mapsto \delta (t) $. Dans ce cas, la réponse que renvoie le système n'est d'autre que la matrice impulsionnelle $G=C e^{At} B$. La réponse impulsionelle de notre système est représentée dans la figure \ref{pulse}:


\begin{figure}[H]
	\centering
	\def\svgwidth{0.90\linewidth}
	\input{pdf/pulse.pdf_tex}
	\caption{\textbf{Réponse impusionnelle du système NXTway-GS}}
	\label{pulse}
\end{figure}

En faite, comme son nom l'indique, la réponse impusionnelle est obtenue en appliquant une brève impulsion aux roues. Cette dernière est représentée dans le graphique de la fonction $t \mapsto \theta (t)$ par le "pique" atteint après un très court instant. Le mouvement est alors aténué par les frottements. 
Suite à ce changement d'état, le corps du Nxtway-GS est projeté en arrière jusqu'à s'écraser à terre. Quant au mouvement de précession, il s'agit d'une rotation à vitesse angulaire décroissante dont le sens dépend de la roue qu'on stimule. On peut remarquer que seul ce dernier mouvement dépend de la roue sur laquelle on agît.
 
  \section{Réponse indicielle}
 
 En guise de petit rappel, la réponse indicielle correspond à la trajectoire observée lorsqu'un contrôle constant ($u(t)=1$) est introduit dans le système dès lors que le temps $t$ est supérieur à zéro, et ce à l'état initial nul.\\
 
 En d'autres termes, cela nous donnera la trajectoire que nous pourrons obtenir si le moteur agit de manière constante sur le système. Voici ce à quoi ressemblera la trajectoire, dès lors que de telles conditions sont établies (\ref{indice}).\\
 
\begin{figure}[H]
	\centering
	\def\svgwidth{0.90\linewidth}
	\input{pdf/indice.pdf_tex}
	\caption{\textbf{Trajectoires d'états liées à la réponse indicielle}}
	\label{indice}
\end{figure}
 
 Nous pouvons à nouveau sans difficultés observer que les angles $theta$ et $psi$ auront tôt fait de commencer à diverger. Nous verrons en revanche que l'angle $phi$, quant à lui, subira une évolution relativement linéaire.\\

Cette évolution (opposée en fonction de la roue) peut être expliquée par le même principe que cité plus haut, liant le fait qu'un même contrôle constant appliqué aux roues (séparément) provoquera une rotation du robot sur lui-même, et ce ici de manière linéaire.\\
 
 Aucun asservissement n'étant appliqué au robot, une tension constante appliquée sur une roue du robot le fera pivoter et tomber.\\
 
 On observera également un overshoot sur l'angle $\theta$, rapidement contré par la chute du robot, l'entraînant dans l'autre sens et provoquant ainsi une divergence dans l'évolution de cet angle.
 
\end{document}

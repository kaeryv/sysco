close all
clc
clear all

%% Syst�me et contr�le: Segway

%% Analyse

r = Robot('nxtwaygs.json');

%A = [0 1; 1 0];
%B = [0; 1];
%C = [-1 1];
%D = 0;
A = r.A;
B = r.B;
C = [1 0 0 0 0 0; 0 1 0 0 0 0; 0 0 1 0 0 0;];
%C = r.C;


% Stabilit� interne 

[vect_p, val_p] = eig(A); %Les col de V sont les vect propres, lambda est une matrice diagonale contentant les vp.

n= size(A,1); 

%Creation des ss espaces stable, antistable et instable.
size_stable = 0;
size_unstable = 0;
size_antistable = 0;
L_antistable = zeros(n);
L_stable = zeros(n);
L_unstable = zeros(n);

for i= 1:n
    if real(val_p(i)) > 0 
        size_antistable = size_antistable + 1;
        L_antistable(:, size_antistable) = vect_p(i);
    elseif real(val_p(i)) >= 0
        size_unstable = size_unstable + 1;
        L_unstable(:, size_unstable) = vect_p(i);
    else 
        size_stable = size_stable + 1;
        L_stable(:, size_stable) = vect_p(i);
    end
end

if size_stable == n
    display('Le syst�me est internement stable')
    stable_int =  true;
    else
    display('Le syst�me n est pas internement stable')
    stable_int = false;
end

disp('Sous-espace antistable:')
if size_antistable == 0
    disp('empty set')
else
    L_antistable = L_antistable(:,1:size_antistable);
    disp(L_antistable)
end

disp('Sous-espace stable:')
if size_stable == 0
    disp('empty set')
else
    L_stable = L_stable(:,1:size_stable);
    disp(L_stable)

end

disp('Sous-espace instable:')
if size_unstable == 0
    disp('empty set')
else
    L_unstable = L_unstable(:,1:size_unstable);
    disp(L_unstable)

end

% Stabilit� externe

R = ss(A,B,C,D); %Construction du syst�me.

trf = tf(R); %Fct de transfert.

trf_s = minreal(trf); %On supprime les p�les en trop.

poletrf = pole(trf_s);

p = size(poletrf,1);



if stable_int
    disp('Le syst�me est externement stable')
else 
    stable_ext = true;
    i = 1;
    while (i <= poletrf) && (stable_ext)
        if poletrf(i) >=0 
            stable_ext = false;
            display('Le syst�me n''est pas externement stable')
        end 
        i = i+1;
    end
end

if stable_ext
    display('Le syst�me est externement stable')
end

% Contr�labilit�

%Matrice de contr�labilot�
cont_mat  = ctrb(R);

%disp('Base du ss-espace contr�lable')
%C_AB = il faut trouver l'image de C


L_cont_mode = zeros(1);
size_cont_mode = 0;
L_incont_mode = zeros(1);
size_incont_mode = 0;

if rank(cont_mat) == n
     display('Le syst�me est compl�tement contr�lable')    
else
    disp('Le syst�me n''est pas compl�tement contr�lable')
end
for i=1:n
    C_mode = [(val_p(i,i) * eye(n) - A) B];
    if rank(C_mode) == n 
        size_cont_mode = size_cont_mode +1;
        L_cont_mode( size_cont_mode) = val_p(i,i); %Contient les modes contr�lables 
    else
        size_incont_mode = size_incont_mode  + 1;
        L_incont_mode( size_incont_mode) = val_p(i,i);
    end 
end

disp('Ensemble des modes contr�lables:')
if size_cont_mode == 0
    disp('empty set')
else
    disp(L_cont_mode)

end

disp('Ensemble des modes incontr�lables:')
if size_incont_mode == 0
    disp('empty set')
else
    disp(L_incont_mode)

end

%Observabilit�

obs_mat = obsv(R);

disp('Base du sous-espace inobservable:')
O_AB = null(obs_mat);
disp(O_AB)

size_obs_mode = 0;
L_obs_mode = zeros(1);
size_inobs_mode = 0;
L_inobs_mode = zeros(1);

if rank(obs_mat) == n 
    display('Le syst�me est compl�tement observable')
else 
    display('Le syst�me n''est pas compl�tement observable')
end

for i=1:n
    O_mode = [(val_p(i,i) * eye(n) - A); C];
     if rank(O_mode) == n
         size_obs_mode = size_obs_mode +1;
         L_obs_mode(size_obs_mode) = val_p(i,i); 
     else
         size_inobs_mode = size_inobs_mode +1;
         L_inobs_mode (size_inobs_mode) = val_p(i,i); 
     end
end


disp('Ensemble des modes observables:')
if size_obs_mode == 0
    disp('empty set')
else
    disp(L_obs_mode)
end

disp('Ensemble des modes inobservables:')
if size_inobs_mode == 0
    disp('empty set')
else
    disp(L_inobs_mode)
end


%D�tectabilit�

if rank(obs_mat) == n %On regarde la condition suffisante (R est CO)
    disp('Le syst�me est d�tectable car il est compl�tement observable.')
else %Tests modaux
    size_DS=0;
    size_NDS=0;
    for i=1:n %Pour chaque valeur propre s, on calcule le rang de [sI-A ; C]
        if rank([val_p(i,i)*eye(n)-A ; C])==n
            size_DS=size_DS+1;
            Detectable_Space(:,size_DS)=vect_p(:,i); %On ajoute le vecteur propre associ� � la valeur propre � l'espace.
        else
            size_NDS=size_NDS+1;
            Undetectable_Space(:,size_NDS)=vect_p(:,i); %On ajoute le vecteur propre associ� � la valeur propre � l'espace.
        end
    end
    disp('Base de l''espace d�tectable:')
    if size_DS == 0
        disp('empty set')
    else
        disp(Detectable_Space)
    end
    
    disp('Base de l''espace non-d�tectable:')
    if size_NDS == 0
        disp('empty set')
    else
        disp(Undetectable_Space)
    end
end

%Stabilisabilit�

if rank(cont_mat) == n %On regarde la condition suffisante (R est CC)
    disp('Le syst�me est stabilisable car il est compl�tement contr�lable.')
else %Tests modaux
    size_SS=0;
    size_NSS=0;
    for i=1:n %Pour chaque valeur propre s, on calcule le rang de [sI-A B]
        if rank([val_p(i,i)*eye(n)-A B])==n
            size_SS=size_SS+1;
            Stabilisable_Space(:,size_DS)=vect_p(:,i); %On ajoute le vecteur propre associ� � la valeur propre � l'espace.
        else
            size_NSS=size_NSS+1;
            Unstabilisable_Space(:,size_NDS)=vect_p(:,i); %On ajoute le vecteur propre associ� � la valeur propre � l'espace.
        end
    end
    
    disp('Base de l''espace stabilisable:')
    if size_SS == 0
        disp('empty set')
    else
        disp(Stabilisable_Space)
    end
    
    disp('Base de l''espace non-stabilisable:')
    if size_NSS == 0
        disp('empty set')
    else
        disp(Unstabilisable_Space)
    end
end

%% Trajectoires d'�tat :
for i=1:n
    x=[];
    index=0;
    x0=vect_p(:,i);
    for t=0:0.1:10
        index=index+1;
        x(:,index)=expm(A*t)*x0;
    end
    
    figure('name','Trajectoire d''�tat')
    suptitle(['Trajectoire d''�tat li�e au mode : ',num2str(i)])
    for j=1:size(C,2)
        subplot(size(C,2),1,j)
        plot([0:0.1:10],x(j,:))
        xlabel('Time (seconds)')
        ylabel('Amplitude')
    end
end



%% R�ponse impulsionnelle

figure('name','r�ponse impulsionnelle')
impulse(R) 


%% R�ponse indicielle

figure('name','r�ponse indicielle')
step(R)

%% Gain statitique
disp('Valeur du gain statique :')
disp(evalfr(trf,0))

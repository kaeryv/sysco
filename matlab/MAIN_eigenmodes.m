clear all; 
close all;
clc; 

addpath control

constants

% Chargement de la représentation du robot
robot = Robot('nxtwaygs.json');
robot.computeMatrices();


[eigenvectors, eigenvalues] = eig(robot.A, 'vector');
disp(eigenvalues) 

enslave(robot, 'lq')
robot.servitude()

% Calcul de l'évolution du mode propre instable asservi
[x, t, u] = eigenmode(robot, 1, 0.0, 0.5);
fig=figure
left_color = [.0 .0 0];
right_color = [0.5 0.5 0.5];
set(fig,'defaultAxesColorOrder',[left_color; right_color]);

yyaxis left
% Theta
plot(t,x(:,1), 'color', colors.red, 'LineWidth', 3, 'LineStyle', '-');
hold on
ylabel('Amplitude de trajectoire [rad]');
% Psi
plot(t,x(:,2), 'color', colors.blue, 'LineWidth', 3, 'LineStyle', '-');
% Phi
plot(t,x(:,3), 'color', colors.green, 'LineWidth', 3, 'LineStyle', '-');
yyaxis right
ylabel('Amplitude du contrôle [V]');
plot(t,u(:,1), 'color', colors.gray, 'LineWidth', 3); hold on
plot(t,u(:,2), 'color', colors.black, 'LineWidth', 3);

set(gca, 'FontSize', 18)
set(gca, 'FontName', 'Times')



title('Réponse à la condition initiale');
xlabel('Temps [s]');
legend('\theta','\psi','\phi', 'u_l','u_r' ,'Interpreter','latex')

clear all; 
close all;
clc; 

addpath control

constants

% Chargement de la représentation du robot
robot = Robot('nxtwaygs.json');
robot.computeMatrices();
slave_type = 'lq';
prefix = ' CORR*'

enslave(robot, slave_type);
robot.servitude();
robot.K

% C defines output
f = fopen(strcat('K_', slave_type, '.h'), 'w');

for i=1:6
  for j=1:2
    fprintf(f,strcat('#define K%i%i ' , prefix,'%f\n'),j,i,robot.K(j,i));
  end
end

% Latex output
f = fopen(strcat('K_', slave_type, '.tex'), 'w');
fprintf(f,'\\begin{pmatrix}\n');
for j=1:2
  for i=1:6
      fprintf(f,strcat(' %f '), robot.K(j,i));
      if i ~=6
        fprintf(f,'&');
      end
  end
  if j ~=2
    fprintf(f,'\\\\');
  end
  fprintf(f,'\n');
end
fprintf(f,'\\end{pmatrix}\n');
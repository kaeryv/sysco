function [x,t, u] = eigenmode(robot, n, tmin, tmax)
  % Nous l'avons implémentée mais en pratique, nous utiliserons la fonction 
  % built - in de matlab pour son scaling automatique du temps

  sys = robot.getControlSystem();
  
  [eigenvectors, eigenvalues] = eig(robot.A, 'vector');
  %sampling = 500;
  
  %t = linspace(tmin, tmax, sampling);
  
  % Recuperons la vp et le Vp
  %lambda = eigenvalues(n);
  v0 = eigenvectors(:,n);
  
  % Stock l'etat de sortie
  %x = zeros(length(t), 6);
  [y, t, x] = initial(sys, v0);
  u = zeros(length(t), 2);
  % Evolution libre pour la [n]eme valeur propre
  for i = 1:length(t)
  %  x(i, :) = transpose(expm(robot.A*(t(i)-tmin))*v0);
    u(i, :) = robot.K * transpose(x(i,:));
  end
end


classdef Robot < handle
  methods
    function obj = Robot(path)
      if and(nargin >= 1, ischar(path))
        if exist(path)
          obj.props = jsondecode(fileread(path));
        else
          error('Model file does not exist')
        end
      else
        error('Value must be valid path.')
      end

      body = obj.props.body;
      wheel = obj.props.wheel;

      wheel.inertia = wheel.mass * wheel.radius^2 / 2.0;
      body.trunk = body.height / 2.0;
      body.pitchInertia = body.mass * body.trunk^2 / 3.0;
      body.yawInertia = body.mass * (body.width^2 + body.depth^2)/ 12.0;

      obj.props.body = body;
      obj.props.wheel = wheel;

      obj.A = zeros(6, 6);
      obj.B = zeros(6, 2);
      obj.C = [ ...
       1 0 0 0 0 0; ...
       0 0 0 0 0 0; ...
       0 0 1 0 0 0; ...
      ];
      

    end 
    
    function computeMatrices(obj)

      obj.A = zeros(6, 6);
      obj.B = zeros(6, 2);
      obj.C = [ ...
       1 0 0 0 0 0; ...
       0 0 0 0 0 0; ...
       0 0 1 0 0 0; ...
      ];
 
      body = obj.props.body;
      wheel = obj.props.wheel;
      motor = obj.props.motor;

      alpha = motor.gearratio * motor.torque / motor.resistance;
      K = (body.width/2/wheel.radius * alpha);
      beta = (motor.gearratio * motor.torque * motor.emf) / motor.resistance  ...
           + motor.bmfriction;
      I = 1/2*wheel.mass*body.width^2 ...
      + body.yawInertia + body.width^2/2/wheel.radius^2 ...
      *(wheel.inertia+motor.gearratio^2*motor.inertia);
      
      J = body.width^2/2/wheel.radius^2 *(beta+motor.wffriction);
      E = zeros(2);
      E(1, 1) = (2.0 * wheel.mass + body.mass) * wheel.radius^2 ...
              + 2 * wheel.inertia + 2 * motor.gearratio^2 * motor.inertia;
      E(1, 2) = body.mass * body.trunk * wheel.radius ...
              - 2 * motor.gearratio^2 * motor.inertia;
      E(2, 1) = E(1, 2);
      E(2, 2) = body.mass * body.trunk^2 + body.pitchInertia ...
              + 2.0 * motor.gearratio^2 * motor.inertia;

      F = zeros(2);
      F(1, 1) = beta + motor.wffriction;
      F(1, 2) = - beta;
      F(2, 1) = - beta;
      F(2, 2) =   beta;
      F = 2 * F; 
      phys.g = 9.81; 
      G = zeros(2);
      G(2, 2) = - body.mass * phys.g * body.trunk;
      
      H = alpha * ones(2);
      H(2, :) = - H(2, :);

      obj.A(5:6, 5:6) = [0, 1; ...
                         0, -J/I];
      obj.A(1:2, 3:4) = eye(2);
      obj.A(3:4, 1:2) = - inv(E) * G;
      obj.A(3:4, 3:4) = - inv(E) * F;

      obj.B(3:4,:) = inv(E) * H;
      obj.B(5:6, 1:2) = [0, 0; ...
                        -K/I K/I];
      % La matrice bloc diagonale formée n'est pas la représentation souhaitée. [\theta, \psi, \phi,
      % \dot\theta, \dot\psi, \dot\phi]
      % Il faut simplementdéplacer la variable \phi à la troisième position, ce qui passe par :
      obj.A = obj.A([1,2,5,3,4,6],[1,2,5,3,4,6]);
      obj.B = obj.B([1,2,5,3,4,6],:);

    end
    function sys = getControlSystem(obj)
      sys = ss(obj.A, obj.B, obj.C, []);
    end

    function servitude(obj)
      obj.A = obj.A + obj.B * obj.K;
    end
  end
  
  properties
    props = {};
    A = [];
    B = [];
    C = [];
    K = [];
  end
end

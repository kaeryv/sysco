addpath extern
addpath control

robot = Robot('nxtwaygs.json');
robot.computeMatrices();
enslave(robot, 'lq');
robot.servitude();

x0 = [0.1; 0.0; 0.1; 0.0; 0.0; 0.0];
t = 0:0.0001:10.0;
x = x0;
xi = zeros([size(x0,1), size(t,2)]);


dt = t(2)-t(1)
for it=1:length(t)
    u = robot.K*x;
    
    dxdt = nonline(t(it), x, robot.props.motor.inertia, robot.props.motor.bmfriction, u);
    x = x + dt*dxdt;
    
    xi(:, it) = x;
end
fig = figure;
for k=1:6
    size(xi(:,k))
    plot(xi(k, :), 'LineStyle', '-')
end
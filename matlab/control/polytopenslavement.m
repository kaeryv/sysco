function polytopenslavement(robot)
    epsilon=1.0;
    J = [(1-epsilon)*robot.props.motor.inertia; (1+epsilon)*robot.props.motor.inertia]; 
    f = [(1-epsilon)*robot.props.motor.wffriction; (1+epsilon)*robot.props.motor.wffriction];
    
    % On initialise le système d'LMIs
    append_lmi()
    m=size(robot.B, 2);
    n=size(robot.A, 1);
    Y = lmivar(2,[m n]);
    W = lmivar(1,[n 1]);


   	R = 2000;
	  alpha = 0.01 ;
	  theta = 85/180*pi; 
      
    for i=1:2
        for j=1:2
            %Redéfinir le robot en fonction de J[i] et f[j], en modifiant
            %le json 
            robot.props.motor.inertia = J(i,1);
            robot.props.motor.wffriction = f(i,1);
            robot.computeMatrices();
            append_lmi(n, m, robot.A, robot.B, W, Y, alpha, R, theta);          
        end
    end
      
    lmiProblem=getlmis;
	  [tmin,Psol]=feasp(lmiProblem);


    if(tmin >= 0E-10)
      error('FAILED to find fitting polytop controller ... Yeah, happens.')
    end

	  Y=dec2mat(lmiProblem,Psol,Y);
	  W=dec2mat(lmiProblem,Psol,W);

	  robot.K=Y/W;

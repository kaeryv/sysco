function append_lmi(n, m, A, B, W, Y, alpha, R, theta)
    %% LMI are a STATE machine, this function appends LMIs to the current LMI stack
    % LMI stack pointer
    persistent sp;

    if nargin == 0
      disp('Init LMI system')
      sp = 1;
      setlmis([]) ;
      return
    end

    % Doit être Définie positive
    lmiterm([-sp 1 1 W], 1 ,1);
    
    sp = sp+1;
    %'s' ecrit le translaté de l expression
    lmiterm([sp 1 1 W],    A, 1, 's');
    lmiterm([sp 1 1 Y],    B, 1, 's');
    lmiterm([-sp 1 1 W], - 2 * alpha, 1) ;
 
    sp = sp+1;
     % R est une cst positive qui contrôle le module de lambda
    lmiterm([sp 1 1 W], - R, 1 );

    lmiterm([sp 1 2 W],   1, A');
    lmiterm([sp 1 2 -Y],  1, B');
    lmiterm([sp 2 1 W],   A, 1 );
    lmiterm([sp 2 1 Y],   B, 1 );
    lmiterm([sp 2 2 W] ,- R, 1 );

    sp = sp+1;
    %theta contrôle le module de la partie imaginaire sur la partie réelle
    lmiterm([sp 1 1 W],   sin(theta) * A,  1, 's'         );
    lmiterm([sp 1 1 Y],   sin(theta) * B,  1, 's'         );

    lmiterm([sp 1 2 W],   cos(theta) * A,  1              );
    lmiterm([sp 1 2 W],   1,             - cos(theta) * A');
    lmiterm([sp 1 2 Y],   cos(theta) * B,  1              );
    lmiterm([sp 1 2 -Y],  1,             - cos(theta) * B');

    lmiterm([sp 2 1 W], - cos(theta) * A,  1              );
    lmiterm([sp 2 1 W],   1,               cos(theta) * A');
    lmiterm([sp 2 1 Y], - cos(theta) * B,  1              );
    lmiterm([sp 2 1 -Y],  1,               cos(theta) * B');

    lmiterm([sp 2 2 W],   sin(theta) * A,  1, 's'         );
    lmiterm([sp 2 2 Y],   sin(theta) * B,  1, 's'         );

    sp = sp+1;
end

function enslave(robot, method)
    if strcmp(method, 'place')
        placeenslavement(robot);
    elseif strcmp(method, 'lmi')
        lmienslavement(robot);
    elseif strcmp(method, 'poly')
        polytopenslavement(robot);
    elseif strcmp(method, 'lq')
        lqenslavement(robot);
    end

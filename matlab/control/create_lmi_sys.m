% ====================================================================================================
% create_lmi_sys.m
% 
% Generates the gain matrix (K) for a given robot using the power of ancien wisdom and LMIs
%
%
% Requires:
% - Control system toolbox
% - Robust control system toolbox
% - Symbolic math toolbox
% - Optimisation toolbox
%
% ====================================================================================================

function [LMISYS, W, Y] = create_lmi_sys(robot, alpha, rho, theta, n, m)

    %Description: Créer le système de LMI en fonction des valeurs de jm, fm, alpha, theha et rob. 
    %m et n permettent d'avoir les dimensions de Y et W

    robot.computeMatrices();
    
    % Reset the lmis env
    append_lmi()
    % Create LMI variables W and Y
    W = lmivar(1, [n 1]);
    Y = lmivar(2, [m n]);
    
    append_lmi(n, m, robot.A, robot.B, W, Y, alpha, rho, theta);
    
    LMISYS = getlmis;
end

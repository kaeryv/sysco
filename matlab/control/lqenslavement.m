function lqenslavement(robot)
    %Minimiser J parmis tous les controle CPM
    %Si (A,B) stabilisable et (C,A) d�tectable, on peut trouver une
    %solution P pour l'�quation de RICCATI
    
    sys=robot.getControlSystem();
    
    %% lq
    m = size(robot.B, 2);
    n = size(robot.A, 1);
    Q = eye(n);
    R = eye(m);
    
    %On d�finit la matrice N comme nulle
    N = zeros(6,2);
    
    [K,S,E]=lqr(sys,Q,R,N);
    %P unique et stabilisante
    %K=R^{-1}B'Q
    
    robot.K = -K;

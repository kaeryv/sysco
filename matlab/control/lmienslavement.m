
% ====================================================================================================
% asservissement.m
% 
% Generates the gain matrix (K) for a given robot using the power of ancien wisdom and LMIs
%
%
% Requires:
% - Control system toolbox
% - Robust control system toolbox
% - Symbolic math toolbox
% - Optimisation toolbox
%
% ====================================================================================================

function lmienslavement(robot)
      m = size(robot.B, 2);
      n = size(robot.A, 2);

      rho = 2000
      alpha = 0.01
      theta = 85/180*pi


      [LMISYS, W, Y] = create_lmi_sys(robot, alpha, rho, theta, n,m);

      [t, x] = feasp(LMISYS);

      valW = dec2mat(LMISYS, x, W);
      valY = dec2mat(LMISYS, x, Y);
      
      robot.K = valY/valW;
end

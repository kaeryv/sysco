close all
clc
clear all


%% Système et contrôle: Segway

%% Analyse

load('robot.mat')
r.computeMatrices()

A = r.A;
B = r.B;

C = [1 0 0 0 0 0; 0 1 0 0 0 0; 0 0 1 0 0 0;];


%stabilité interne 


[vect_p, val_p] = eig(A); %Les col de V sont les vect propres, lambda est une matrice diagonale contentant les vp.

n= size(A,1); 

%Creation des ss espaces stable, antistable et instable.
size_stable = 0;
size_unstable = 0;
size_antistable = 0;
L_antistable = zeros(n);
L_stable = zeros(n);
L_unstable = zeros(n);

for i= 1:n
    if real(val_p(i,i)) > 0 
        size_antistable = size_antistable + 1;
        L_antistable(:, size_antistable) = vect_p(:,i);
    end
    if real(val_p(i,i)) >= 0
        size_unstable = size_unstable + 1;
        L_unstable(:, size_unstable) = vect_p(:,i);
    else 
        size_stable = size_stable + 1;
        L_stable(:, size_stable) = vect_p(:,i);
    end
end

if size_stable == n
    display('Le système est internement stable')
    stable_int =  true;
else
    display('Le système n''est pas internement stable')
    stable_int = false;
end

disp('Sous-espace antistable:')
if size_antistable == 0
    disp('empty set')
else
    L_antistable = L_antistable(:,1:size_antistable);
    disp(L_antistable)
end

disp('Sous-espace stable:')
if size_stable == 0
    disp('empty set')
else
    L_stable = L_stable(:,1:size_stable);
    disp(L_stable)
end

disp('Sous-espace instable:')
if size_unstable == 0
    disp('empty set')
else
    L_unstable = L_unstable(:,1:size_unstable);
    disp(L_unstable)
end

% Stabilité externe

sys = ss(A,B,C,[]);

trf = tf(sys); %Fct de transfert.

trf_s = minreal(trf); %On supprime les pôles en trop.

poletrf = pole(trf_s);


n_badpoles = size(poletrf(real(poletrf) >= 0));
n_badpoles = n_badpoles(:, 1);

if stable_int
    disp('Le système est externement stable')

else
  if n_badpoles > 0
    disp(strcat('Le système n''est pas externement stable : ', num2str(n_badpoles), ... 
    ' poles dans le plan C+'));
  else
      disp('Le système est externement stable')
  end
end


% Contrôlabilité

%Matrice de contrôlabiloté


controlability_matrix = ctrb(sys);
controlable_rank = rank(controlability_matrix);

disp(strcat('Espace controllable est R', num2str(controlable_rank)));
%controlability_matrix_base = colspace(sym(controlability_matrix));
cont_mat = controlability_matrix;


if rank(cont_mat) == n
     display('Le système est complétement contrôlable')
else
    C_AB = zeros(n);
    size_C_AB = 0;
    p = size(B,2);

    L_cont_mode = zeros(1);
    size_cont_mode = 0;
    L_incont_mode = zeros(1);
    size_incont_mode = 0;

    disp('Le système n''est pas complètement contrôlable')

    for i=1:n
        C_mode = [(val_p(i,i) * eye(n) - A) B];
        if rank(C_mode) == n 
            size_cont_mode = size_cont_mode +1;
            L_cont_mode( size_cont_mode) = val_p(i,i); %Contient les modes contrôlables 
        else
            size_incont_mode = size_incont_mode  + 1;
            L_incont_mode( size_incont_mode) = val_p(i,i);
        end 
    end
%    disp('Base du ss-espace contrôlable')
%    i=1;
%    while i<= n * p
%        size_C_AB = size_C_AB + 1;
%        C_AB(:,size_C_AB) = cont_mat(:,size_C_AB);
%        i = i +1;
%    end

%    disp(C_AB)

    disp('Ensemble des modes contrôlables:')
    if size_cont_mode == 0
        disp('empty set')
    else
        disp(L_cont_mode)
    end

    disp('Ensemble des modes incontrôlables:')
    if size_incont_mode == 0
        disp('empty set')
    else
        disp(L_incont_mode)
    end
end
%Observabilité

obs_mat = obsv(sys);

if rank(obs_mat) == n 
    display('Le système est complétement observable')
else 
    size_obs_mode = 0;
    L_obs_mode = zeros(1);
    size_inobs_mode = 0;
    L_inobs_mode = zeros(1);


    display('Le système n''est pas complétement observable')


    disp('Base du sous-espace inobservable:')
    O_AB = null(obs_mat);
    disp(O_AB)

    for i=1:n
        O_mode = [(val_p(i,i) * eye(n) - A); C];
         if rank(O_mode) == n
             size_obs_mode = size_obs_mode +1;
             L_obs_mode(size_obs_mode) = val_p(i,i); 
         else
             size_inobs_mode = size_inobs_mode +1;
            L_inobs_mode (size_inobs_mode) = val_p(i,i); 
         end
    end


    disp('Ensemble des modes observables:')
    if size_obs_mode == 0
        disp('empty set')
    else
        disp(L_obs_mode)
    end

    disp('Ensemble des modes inobservables:')
    if size_inobs_mode == 0
        disp('empty set')
    else
        disp(L_inobs_mode)
    end
    
end

%Détectabilité

if rank(obs_mat) == n %On regarde la condition suffisante (R est CO)
    disp('Le système est détectable car il est complètement observable.')
else %Tests modaux
    size_DS=0;
    size_NDS=0;
    Detectable_Space = zeros(n);
    Undetectable_Space = zeros(n);
    
    for i=1:n %Pour chaque valeur propre s, on calcule le rang de [sI-A ; C]
        if rank([val_p(i,i)*eye(n)-A ; C])==n
            size_DS=size_DS+1;
            Detectable_Space(:,size_DS)=vect_p(:,i); %On ajoute le vecteur propre associé à la valeur propre à l'espace.
        else
            size_NDS=size_NDS+1;
            Undetectable_Space(:,size_NDS)=vect_p(:,i); %On ajoute le vecteur propre associé à la valeur propre à l'espace.
        end
    end
    disp('Base du sous-espace détectable:')
    if size_DS == 0
        disp('empty set')
    else
        disp(Detectable_Space(:,1:size_DS))
    end
    
    disp('Base du sous-espace indétectable:')
    if size_NDS == 0
        disp('empty set')
    else
        disp(Undetectable_Space(:,1:size_NDS))
    end
end

%Stabilisabilité

if rank(cont_mat) == n %On regarde la condition suffisante (R est CC)
    disp('Le système est stabilisable car il est complètement contrôlable.')
else %Tests modaux
    size_SS=0;
    size_NSS=0;
    Stabilisable_Space = zeros(n);
    Unstabilisable_Space = zeros(n);
    for i=1:n %Pour chaque valeur propre s, on calcule le rang de [sI-A B]
        if rank([val_p(i,i)*eye(n)-A B])==n
            size_SS=size_SS+1;
            Stabilisable_Space(:,size_DS)=vect_p(:,i); %On ajoute le vecteur propre associé à la valeur propre à l'espace.
        else
            size_NSS=size_NSS+1;
            Unstabilisable_Space(:,size_NDS)=vect_p(:,i); %On ajoute le vecteur propre associé à la valeur propre à l'espace.
        end
    end
    
    disp('Base du sous-espace stabilisable:')
    if size_SS == 0
        disp('empty set')
    else
        disp(Stabilisable_Space(:,1:size_SS))
    end
    
    disp('Base du sous-espace instabilisable:')
    if size_NSS == 0
        disp('empty set')
    else
        disp(Unstabilisable_Space(:,1:size_NSS))
    end
end


       
 


%% Nettoyage de l'espace de travail
clear all; 
close all;
clc; 

addpath control

constants

% Chargement de la représentation du robot
robot = Robot('nxtwaygs.json');
robot.computeMatrices();
slave_type = 'place'

[eigenvectors, eigenvalues] = eig(robot.A, 'vector');
disp(eigenvalues) 

enslave(robot, slave_type)
robot.servitude()

vp = eig(robot.A)

% ONLY FOR DIPLAY
rho = 2000;
alpha = 0.01;
theta = 85/180*pi;

fig=figure

viscircles([0,0],rho, 'color', colors.gray); hold on
plot([-alpha, -alpha], [-rho, +rho], 'LineWidth', 2)
plot([-rho, 0], -tan(theta)*[-rho, 0], 'k-', 'LineWidth', 2)
plot([-rho, 0], tan(theta)*[-rho, 0], 'k-', 'LineWidth', 2)
axis([-rho, rho, -rho, rho])

% We plot eigenvalues
for(i=1:length(vp))
  plot(real(vp(i)), imag(vp(i)), 'ro', 'MarkerFaceColor',[1,0,0])
end
axis equal
xlabel('Real part', 'Interpreter', 'latex')
ylabel('Imag part', 'Interpreter', 'latex')
title(strcat('Repartition des valeurs propres via ', slave_type), 'Interpreter', 'latex')

set(gca, 'FontSize', 12)
set(gca, 'FontName', 'Times')
grid on



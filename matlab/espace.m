
%% Nettoyage de l'espace de travail
clear all; 
close all;
clc; 

constants

%% Chargement de la représentation du robot
robot = Robot('nxtwaygs.json');
robot.computeMatrices();

% Matrice C moins exigeante en capteurs
robot.C = [ ...
       1 0 0 0 0 0; ...
       0 1 0 0 0 0; ...
       0 0 1 0 0 0; ...
      ];
%r.C = [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1;];
%r.C = zeros(6);
sys = robot.getControlSystem();


% Calcul des valeurs propres et des espaces de stabilité
[eigenvectors, eigenvalues] = eig(robot.A, 'vector');

% L'espace stable correspond aux parties réeles strictement négatives
stables_ids   = real(eigenvalues) <  0;
% L'espace instable correspond aux parties réeles positives ou nulleis
unstable_ids  = real(eigenvalues) >= 0;
% L'espace antistable correspond aux parties réeles strictement positives
astable_ids   = real(eigenvalues) >  0;


%% Sous-espace controlable
crtb.matrix = ctrb(sys);
crtb.rank = rank(crtb.matrix);

disp(strcat('L''espace controllable est |R', num2str(crtb.rank)));

%% Sous-espace observable
obsv.matrix = obsv(sys);
obsv.rank = rank(obsv.matrix);

disp(strcat('L''espace controllable est |R', num2str(obsv.rank)));


pause;

transfer.func = tf(sys);
transfer.simpfunc = minreal(transfer.func);
transfer.poles = pole(transfer.simpfunc);

% Fonction de transfert
transfer.simpfunc
pause


%% Nettoyage de l'espace de travail
clear all; 
close all;
clc; 

constants

%% Chargement de la représentation du robot
robot = Robot('nxtwaygs.json');
robot.computeMatrices();

% Matrice C moins exigeante en capteurs
robot.C = [ ...
       1 0 0 0 0 0; ...
       0 1 0 0 0 0; ...
       0 0 1 0 0 0; ...
      ];
%r.C = [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1;];
%r.C = zeros(6);
sys = robot.getControlSystem();


% Calcul des valeurs propres et des espaces de stabilité
[eigenvectors, eigenvalues] = eig(robot.A, 'vector');

% L'espace stable correspond aux parties réeles strictement négatives
stables_ids   = real(eigenvalues) <  0;
% L'espace instable correspond aux parties réeles positives ou nulleis
unstable_ids  = real(eigenvalues) >= 0;
% L'espace antistable correspond aux parties réeles strictement positives
astable_ids   = real(eigenvalues) >  0;


%% Sous-espace controlable
crtb.matrix = ctrb(sys);
crtb.rank = rank(crtb.matrix);

disp(strcat('L''espace controllable est |R', num2str(crtb.rank)));

%% Sous-espace observable
obsv.matrix = obsv(sys);
obsv.rank = rank(obsv.matrix);

disp(strcat('L''espace controllable est |R', num2str(obsv.rank)));


pause;

transfer.func = tf(sys);
transfer.simpfunc = minreal(transfer.func);
transfer.poles = pole(transfer.simpfunc);

% Fonction de transfert
transfer.simpfunc
pause



clear all
close all
clc

addpath control

% We create a new robot to play with
robot = Robot('nxtwaygs.json');

% Here we can override any value describing the robot
% ...

% We need to generate its system of linear equations
robot.computeMatrices(); 

% Here we can override the values of any matrix inside robot
% ...


% We enslave the robot
enslave(robot, 'lmi');

disp(robot.K);


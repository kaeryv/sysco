%% Nettoyage de l'espace de travail
clear all; 
close all;
clc; 


colors.red = [136,70,70]/255.0;
colors.green = [70,136,92]/255.0;
colors.blue = [70,89,136]/255.0;


%% Chargement de la représentation du robot
robot = Robot('nxtwaygs.json');
robot.computeMatrices();

% Matrice C moins exigeante en capteurs
% On ne capte que la valeur des angles
robot.C = [ ...
       1 0 0 0 0 0; ...
       0 1 0 0 0 0; ...
       0 0 1 0 0 0; ...
      ];
%r.C = [0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1;];
%r.C = zeros(6);
sys = robot.getControlSystem();


[y,t] = impulse(sys, linspace(0,0.4,500));


subplot(3,1,1)
plot(t, y(:,1,1)', 'color', colors.red, 'LineWidth', 2);
hold on;
plot(t, y(:,1,2)', 'color', colors.red, 'LineWidth', 3, 'LineStyle', '--');
title('Réponse impulsionnelle - \theta');
legend('Impulsion de tension sur roue gauche', 'Impulsion de tension sur roue droite')
ylabel('Amplitude');

subplot(3,1,2)
plot(t, y(:,2,1)', 'color', colors.blue, 'LineWidth', 2);
hold on;
plot(t, y(:,2,2)', 'color', colors.blue, 'LineWidth', 3, 'LineStyle', '--');
title('Impulse response - \psi');
legend('Impulsion de tension sur roue gauche', 'Impulsion de tension sur roue droite')
ylabel('Amplitude');

subplot(3,1,3)
plot(t, y(:,3,1)', 'color', colors.green, 'LineWidth', 3);
hold on;
plot(t, y(:,3,2)', 'color', colors.green, 'LineWidth', 3, 'LineStyle', '--');
title('Impulse response - \phi');
legend('Impulsion de tension sur roue gauche', 'Impulsion de tension sur roue droite')
xlabel('Temps [s]');
ylabel('Amplitude');
